(IM)=
# L'Indice Majoré (IM)

L'indice majoré est la valeur utilisé pour le calcul du traitement.

L'agent rentre son corp, son grade et son échelon. Le logiciel en déduit l'Indice Majoré en utilisant {ref}`params/grilles:les grilles indiciaires`.