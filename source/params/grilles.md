# Les grilles indiciaires

Cette pages référence l'ensemble des grilles indiciaires de la DGAC

:Référence législative: [Décret N°2022-994 du 7/1/2022](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000046026212) \
:Vérifié le: 5/6/2023


## Emploi fonctionnel

### CUTAC

### CSTAC

### CTAC

## Corps Administratif DGAC

### ADAAC

### ASAAC

## Corps Techniques DGAC

### TSEEAC

### IESSA

| Grade | Echelon | Durée | Indice Brut |
|:-----:|:-------:|:-----:|:-----------:|
| en chef | 6.3 | - | HEA 3|
|         | 6.2 | - | HEA 2|
|         | 6.1 | - | HEA 1|
| | 5 | ...




:Référence législative: [Décret N°91-56 du 16 janvier 1991 (le statut)](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000006077105) & [Décret n° 2009-1322 du 27 octobre 2009 (les indices)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000021213692/) & [(corespondance IB<->IM)] ()
:Vérifié le: 5/6/2023

### ICNA

| Grade | Echelon | Durée | Indice Brut |
|:-----:|:-------:|:-----:|:-----------:|
|en chef| 7e | - |HEA|
| | 6e | 1 an | 1027|
| | 5e | 1 an 6 mois|995|
| | 4e | 1 an 6 mois|946|
| | 3e | 1 an 6 mois|890|
| | 2e | 2 ans | 837|
| | 1er | 2 ans | 784 |

:Référence législative: [Décret n°90-998 du 8 novembre 1990 (le statut)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000708880) & [Décret n° 2009-1322 du 27 octobre 2009 (les indices)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000021213692/) 

### IEEAC

## Corps Ministériel